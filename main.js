import fs from "fs";
import _ from "lodash";
import translate from "translate";
import inquirer from "inquirer";

const result = [];
translate.engine = "google"; // Or "yandex", "libre", "deepl"
translate.key = process.env.GOOGLE_KEY;

const answares = await inquirer.prompt([
    {
        type: "input",
        name: "readFile",
        message: "when is file for read from it(Enter file path): ",
        validate: (input) => {
            if (fs.existsSync(input)) {
                return true;
            }
            return "The input path is invalid";
        },
    },
    {
        type: "input",
        name: "writeFile",
        message: "Enter the name for write export in that: ",
        validate: (input) => {
            if (input.length < 3) {
                return "The input data is invalid";
            }
            return true;
        },
    },
    // {
    //     type: "input",
    //     name: "coloumn",
    //     message: "Enter the name of coloumn for translate it: ",
    //     validate: (input) => {
    //         if (input.length < 3) {
    //             return "The input data is invalid";
    //         }
    //         return true;
    //     },
    // },
]);

const data = fs.readFileSync(answares.readFile, "utf-8");

const final = fs.createWriteStream(answares.writeFile, "utf-8");

_.forEach(JSON.parse(data), async function (value) {
    try {
        const text = await translate(value.fa_name, {
            from: "fa",
            to: "en",
        });
        value.en_name = text;
        final.write(JSON.stringify(value) + ",\n", (err) => {
            if (err) {
                console.log(err);
            }
        });
    } catch {
        console.log("We have an error");
    }
});

console.log("Processing...");
